﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class References : MonoBehaviour
{

    //Variables to store all information, hidden from designer
    public Text question;
    public Button an1;
    public Button an2;
    public Text score;
    [HideInInspector]
    public List<string> questions = new List<string>();
    [HideInInspector]
    public List<string> answer1 = new List<string>();
    [HideInInspector]
    public List<string> answer2 = new List<string>();

    void Gather()
    {
        questions = GetComponent<LoadScript>().questions;
        answer1 = GetComponent<LoadScript>().answer1;
        answer2 = GetComponent<LoadScript>().answer2;
        SendMessage("Begin");
    }

}
