﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Collections.Generic;

[RequireComponent(typeof(References))]
public class LoadScript : MonoBehaviour
{
    FileInfo originalFile;
    TextAsset textFile;
    TextReader reader;

    public List<string> questions = new List<string>();
    public List<string> answer1 = new List<string>();
    public List<string> answer2 = new List<string>();
    // Use this for initialization
    void Start()
    {
        originalFile = new System.IO.FileInfo(Application.dataPath + "/questions.txt");

        if (originalFile != null && originalFile.Exists)
        {
            reader = originalFile.OpenText();
        }
        else
        {
            textFile = (TextAsset)Resources.Load("embedded", typeof(TextAsset));
            reader = new StringReader(textFile.text);
        }

        string lineOfText;
        int lineNumber = 0;

        while ((lineOfText = reader.ReadLine()) != null)
        {
            if (lineNumber % 3 == 0)
            {
                questions.Add(lineOfText);
            }
            else if(lineNumber % 3 == 1)
            {
                answer1.Add(lineOfText);
            }
            else
            {
                answer2.Add(lineOfText);
            }

            lineNumber++;
        }

        SendMessage("Gather");
    }

    // Update is called once per frame
    void Update()
    {

    }
}