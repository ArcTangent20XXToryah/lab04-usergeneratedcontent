﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

//Force unity to add the references script if it is not already on the object
[RequireComponent(typeof(References))]
public class Engine : MonoBehaviour {
    
    //Variables
    References refs;
    int points;
    string ques = " "; 
    string ans1 = " ";                   
    string ans2 = " "; 
    public bool ans1True;                         
    int totalQuestions = 0;
    int correct = 0;

    void Begin()
    {
        refs = gameObject.GetComponent<References>();
        StartGame();
    }

    public void _Answer1()
    {
        if (ans1True == true)
        {
            correct++;
            totalQuestions++;
            StartGame();
        }
        else
        {
            totalQuestions++;
            StartGame();
        }
    }

    public void _Answer2()
    {
        if (ans1True == false)
        {
            correct++;
            totalQuestions++;
            StartGame();
        }
        else
        {
            totalQuestions++;
            StartGame();
        }
    }


    public void StartGame()
    {
        

        int numSent = refs.questions.Count;
        int randomSent = Random.Range(0, numSent);

        ques = refs.questions[randomSent];
        ans1 = refs.answer1[randomSent];
        ans2 = refs.answer2[randomSent];

        int randomTrue = Random.Range(0, 1000);
        if (randomTrue % 2 == 0)
        {
            refs.an1.GetComponentInChildren<Text>().text = ans1.ToString();
            refs.an2.GetComponentInChildren<Text>().text = ans2.ToString();
            ans1True = true;
        }
        else
        {
            refs.an2.GetComponentInChildren<Text>().text = ans1.ToString();
            refs.an1.GetComponentInChildren<Text>().text = ans2.ToString();
            ans1True = false;
        }

        refs.question.text = ques.ToString();
        refs.score.text = "Correct answers: " + correct + "\nTotal questions : " + totalQuestions;
    }

    
}
